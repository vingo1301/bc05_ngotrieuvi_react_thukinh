import React, { Component } from 'react'

export default class 
 extends Component {
    renderGlasses = () => {
      let glass = [];
        let img = "./glassesImage/g1.jpg";
        for (let index = 1; index <10; index ++){
            img = `./glassesImage/g${index}.jpg`;
            let innerHTML = <button onClick={() => this.props.changeGlass(index)} className='col-2 btn'><img src={img} alt="" /></button>;
            glass.push(innerHTML);
        }
      return glass;
    }
  render() {
    return (
      <div className='glassList row'>
        {this.renderGlasses()}
      </div>
    )
  }
}
