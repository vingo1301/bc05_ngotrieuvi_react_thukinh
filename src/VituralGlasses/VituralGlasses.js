import React, { Component } from 'react'
import Glasses from './Glasses'
import Model from './Model'

export default class 
 extends Component {
  state = {
    img: "./glassesImage/v0.png"
  }
  changeGlass = (idGlass) => {
    this.setState({
      img: `./glassesImage/v${idGlass}.png`,
    })
  }
  render() {
    return (
      <div className='container'>
        <Model></Model>
        <Glasses changeGlass = {this.changeGlass}></Glasses>
        <img className='glass' src={this.state.img} alt="" />
      </div>
    )
  }
}
