import logo from './logo.svg';
import './App.css';
import VituralGlasses from './VituralGlasses/VituralGlasses';

function App() {
  return (
    <div className="App">
      <VituralGlasses></VituralGlasses>
    </div>
  );
}

export default App;
